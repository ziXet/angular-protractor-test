

import { HomePage } from './pages/home.page';

describe('test 2', () => {
	const homePage = new HomePage();

	describe('home page should work fine', () => {
		beforeAll(() => {
			homePage.getPage();
		});

		it('should have right title', () => {
			homePage.getPageTitle()
				.then((title: string) => {
					expect(title).toEqual('Conduit');
				});
		});
	})
});



